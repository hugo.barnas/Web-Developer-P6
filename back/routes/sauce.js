//Importation de la methode Express pour faire les routes
const express = require('express');
//Imporation de la methode Router pour faire les routes depuis Express
const router = express.Router();
//Récupération des méthodes
const sauceCtrl = require("../controllers/sauce");
const likeCtrl = require("../controllers/like");
//Récupération du middleware Authorize
const auth = require("../middleware/auth");
//Récupération du multer
const multer = require("../middleware/multer-config");

//Création des routes
//Création d'une route POST
router.post('/', auth, multer, sauceCtrl.createSauce);

//Récupération d'une sauce par son id
router.get('/:id', auth, sauceCtrl.getOneSauce);

//Création de la route qui permet de récupérer toutes les sauces
router.get('/', auth, sauceCtrl.getAllSauces);

//Route pour modifier une sauce par son propriétaire
router.put('/:id', auth, multer, sauceCtrl.modifySauce);

//Route pour supprimer une sauce par son propriétaire
router.delete('/:id', auth, multer, sauceCtrl.deleteSauce);

//Route pour liker une sauce
router.post("/:id/like", auth, multer, likeCtrl.createSauceLike);


module.exports = router;