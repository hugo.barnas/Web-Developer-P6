// const bodyParser = require('body-parser');
//importation du serveur express
const express = require('express');

const app = express();
//Importation de la methode mongoose
const mongoose = require("mongoose");

//Accès au path du serveur
const path = require("path");

//importation des routes
//import de la route sauce
const sauceRoutes = require('./routes/sauce');
//import de la route user
const userRoutes = require('./routes/user');


//Connect à la base de données
mongoose.connect('mongodb+srv://Hugo:Golgot22@cluster0.iw5whcg.mongodb.net/?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

//Middleware pour résoudre le problème des CORS

// app.use(bodyParser.json());

//Middlewares
//CORES
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});

//Pour accéder au corps de la requête 
app.use(express.json());

//Permet à Express de savoir qu'il faut gérer la ressouce images de manière statique.
app.use('/images', express.static(path.join(__dirname, 'images')));

//Route User
app.use('/api/auth', userRoutes);
app.use('/api/sauces', sauceRoutes);


module.exports = app;