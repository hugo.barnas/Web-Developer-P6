//Connexion à Mongoose
const mongoose = require('mongoose');
//Inport de la methode pour s'assure que l'email utilisé est unique
const uniqueValidator = require('mongoose-unique-validator');

//Création du schema
const userSchema = mongoose.Schema({
    email: {
        type: String, required: true, unique: true
    },
    password: {
        type: String, required: true
    }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);
