//importation du modèle Sauce
const Sauce = require("../models/sauce");

//Logique métier pour ajouter un like
exports.createSauceLike = (req, res, next) => {

    //Afficher le req.body : id + like
    console.log("contenue req body :", req.body)
    //Afficher que l'id
    console.log("id :", req.body.userId);
    //Afficher que le like
    console.log("like :", req.body.like);

    //Récupérer l'id de la sauce
    console.log("id de la sauce :", req.params.id);

    //Mise au format en _id
    console.log({ _id: req.params.id });

    //Aller chercher les données de l'objet dans la base de données
    Sauce
        //Récupérer via la méthode findOne l'identifiant de l'objet
        .findOne({ _id: req.params.id })
        //Si l'identifiant est trouvé alors afficher les données 
        .then(sauce => {

            console.log("Le userId est inclu dans le tableau des disliked :", sauce.usersLiked.includes(req.body.userId));
            console.log("le like est à :", req.body.like);

            if (!sauce.usersLiked.includes(req.body.userId) && req.body.like === 1) {
                //Mise à jour de la base de données
                Sauce.updateOne(
                    {
                        //identifiant de la sauce
                        _id: req.params.id
                    },
                    //Modification de la sauce
                    {
                        $inc: { likes: 1 },
                        $push: { usersLiked: req.body.userId }
                    },

                )
                    .then(() => res.status(201).json({ message: "like enregistré" }))
                    .catch(error => {
                        res.status(500).json({ error });
                    })
            }

            //Après un like à 1 on met un like à 0
            if (sauce.usersLiked.includes(req.body.userId) && req.body.like === 0) {
                //Mise à jour de la base de données
                Sauce.updateOne(
                    {
                        //identifiant de la sauce
                        _id: req.params.id
                    },
                    //Modification de la sauce
                    {
                        $inc: { likes: -1 },
                        $pull: { usersLiked: req.body.userId }
                    },

                )
                    .then(() => res.status(201).json({ message: "like 0 enregistré" }))
                    .catch(error => {
                        res.status(500).json({ error });
                    })
            }

            //Après un like = 0, on met un like =-1
            if (!sauce.usersDisliked.includes(req.body.userId) && req.body.like === -1) {
                //Mise à jour de la base de données
                Sauce.updateOne(
                    {
                        //identifiant de la sauce
                        _id: req.params.id
                    },
                    //Modification de la sauce
                    {
                        $inc: { dislikes: 1 },
                        $push: { usersDisliked: req.body.userId },
                        // $pull: { usersLiked: req.body.userId }
                    },

                )
                    .then(() => res.status(201).json({ message: "dislike +1 enregistré" }))
                    .catch(error => {
                        res.status(500).json({ error });
                    })
            }


            //Après un like à -1 on met un like à 0
            if (sauce.usersDisliked.includes(req.body.userId) && req.body.like === 0) {
                //Mise à jour de la base de données
                Sauce.updateOne(
                    {
                        //identifiant de la sauce
                        _id: req.params.id
                    },
                    //Modification de la sauce
                    {
                        $inc: { dislikes: +1 },
                        $pull: { usersDisliked: req.body.userId },
                    },

                )
                    .then(() => res.status(201).json({ message: "like 0 enregistré" }))
                    .catch(error => {
                        res.status(500).json({ error });
                    })
            }


        }
        )
        .catch(error => {
            res.status(500).json({ error });
        });
};