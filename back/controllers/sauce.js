//importation du modèle Sauce
const Sauce = require("../models/sauce");

//importation de la méthode fs pour accéder aux fonctions qui permettent de modifier le système de fichier
const fs = require("fs");

//Logique métier pour créer une sauce
exports.createSauce = (req, res, next) => {
    const sauceObject = JSON.parse(req.body.sauce);
    delete sauceObject._id;
    delete sauceObject._userId;
    const sauce = new Sauce({
        ...sauceObject,
        userId: req.auth.userId,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });

    sauce.save()
        .then(() => { res.status(201).json({ message: 'Sauce enregistré !' }) })
        .catch(error => { res.status(400).json({ error }) })
};


//Logique métier pour récupérer toutes les sauces
exports.getAllSauces = (req, res, next) => {
    Sauce.find()
        .then(things => res.status(200).json(things))
        .catch(error => res.status(400).json({ error }));
};

//Logique métier pour récupére une sauce
exports.getOneSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
        .then(thing => res.status(201).json(thing))
        .catch(error => res.status(404).json({ error }));
};

//Logique métier pour supprimer une sauce 
exports.deleteSauce = (req, res, next) => {
    Sauce.findOne({ _id: req.params.id })
        .then(sauce => {
            //Vérifier que c'est bien le propriétaire de l'objet qui demande la suppression
            //Vérifier que l'userId enregistré en base correspond bien au userId récupéré du tokent
            //si ce n'est pas la cas renvoie d'un message d'erreur
            if (sauce.userId != req.auth.userId) {
                res.status(401).json({ message: 'Not authorized' });
            } else {
                //Si bon utilisateur
                //Récupérer du nom de fichier
                const filename = sauce.imageUrl.split('/images/')[1];
                //Utilisation de la méthode unlink de fs avec le chemin 
                //Suppression du fichier dans la base de données
                fs.unlink(`images/${filename}`, () => {
                    Sauce.deleteOne({ _id: req.params.id })
                        .then(() => { res.status(200).json({ message: 'Objet supprimé !' }) })
                        .catch(error => res.status(401).json({ error }));
                });
            }
        })
        .catch(error => {
            res.status(500).json({ error });
        });
};

//Logique métier pour modifier une sauce
exports.modifySauce = (req, res, next) => {
    const sauceObject = req.file ? {
        ...JSON.parse(req.body.sauce),
        //url de l'image : http://chemin-de-l-image.nom-de-l-extension
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    } : { ...req.body };

    delete sauceObject._userId;
    Sauce.findOne({ _id: req.params.id })
        .then((sauce) => {
            //Vérifier que c'est bien le propriétaire de l'objet qui demande la suppression
            //Vérifier que l'userId enregistré en base correspond bien au userId récupéré du tokent
            //si ce n'est pas la cas renvoie d'un message d'erreur
            if (sauce.userId != req.auth.userId) {
                res.status(401).json({ message: 'Not authorized' });
            } else {
                Sauce.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id })
                    .then(() => res.status(200).json({ message: 'Objet modifié!' }))
                    .catch(error => res.status(401).json({ error }));
            }
        })
        .catch((error) => {
            res.status(400).json({ error });
        });
};

