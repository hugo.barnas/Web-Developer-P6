# The project

This P6 project of openclassrooms is to order to learn API REST with CRUD

# Technology used
## back
### server
- express
- nodemon
- node.js
- js

### Authorization 
- Jsonwebtoken
- mongoose unique validator

### Security
- bcrypt
- dotenv

### Database
- mongoose

# How run this project ?

1. Git clone this project
2. In the front-end part

```
cd back
npm init
npm run start
```

Run the front-end in using :
<http://localhost:4200/>

3. In the back-end part

```
cd back
npm init
nodemon serve
```
Run the front-end in using :
<http://localhost:3000/>


# Results

## Login

![sauce-login](/uploads/0e588372e772fd412e3c186f7d8f57fc/sauce-login.gif)

## Add sauce

![sauce-ajout-sauce](/uploads/bf688dd8314084b1322c188d67638360/sauce-ajout-sauce.gif)


## Add like

![sauce-like](/uploads/4d6d783fbeb3b5398851512c72cc9e76/sauce-like.gif)
